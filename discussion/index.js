// [SECTION] Introduction to Arrays

	//practice: create/declare multiple variables
	//ctrl button + select the lines using your cursor
    let student1 = 'Martin';	
    let student2 = 'Maria';   
    let student3 = 'John';    
    let student4 = 'Ernie';    
    let student5 = 'Bert';  

    //store the following values inside a single container.
    //to declare an array => we simply use an 'array literal' or Square bracket '[]'
    //use '=' => assignment operator to repackage the structure inside the variable
    //be careful when selecting variable names.
    let batch = ['Martin', 'Maria', 'John', 'Ernie', 'Bert'];
    console.log(batch); 

    //create an array that will contain different computerBrands
    let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu']; 

    // '' or " "  => both are used to declare a string data type in JS. 
    //[SIDE LESSON]
    
    //SYNTAX
    //=> 'apple' === "apple"
    // 'apple' //correct
    // "apple" //correct
    // 'apple" //incorrect

    //HOW TO CHOOSE? 

    //=> Use case of the data

    //1. if youre going to use quotations, dialog inside a string.

    //if you will use single quotations when declaring such values like below, it will result to premarturely end the statement.
    let message = "Using Javascript's Array Literal"; 
    console.log(message); 

    let lastWords = '"I Shall Return!" - MacArthur';
    console.log(lastWords); 

    //NOTE: when selecting the correct quotation type, you can use them as an [ESCAPE TOOL] to properly declare expressions within a string.

    //[ALTERNATIVE APPROACH] "\" forward slash to insert quotations.

    //example: 

    let message2 = 'Using Javascript\'s Array Literal'; 
    console.log(message2); 



    //('\') this can be use to escape both a single/double quotion expressions.

    //NOTE: a lot of developers prefer the use of '' (single quotations) as it is easier/simpler to use when declaring string data. (AGAIN, take note of the use cases.)

    
    //Practing the use or git branches
    //ADD this new line of code 
    //add another message

    //crush ko si _______. 
    
